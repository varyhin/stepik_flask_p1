from flask import Flask
from flask import render_template
from data.data import title, subtitle, description, departures, tours

app = Flask(__name__)

@app.route('/')
def main():
    return render_template('index.html', \
        title=title, \
        subtitle=subtitle, \
        description=description, \
        departures=departures, \
        tours=tours)

@app.route('/departure/<id>/')
def departure(id):
    return render_template('departure.html',\
        id=id, \
        title=title, \
        subtitle=subtitle, \
        description=description, \
        departures=departures, \
        tours=tours)

@app.route('/tour/<id>/')
def tour(id):

    #print (id, type(id))

    app.logger.info('Выбран тур %s', id)

    return render_template('tour.html',\
        id=int(id), \
        title=title, \
        subtitle=subtitle, \
        description=description, \
        departures=departures, \
        tours=tours)

#app.run('0.0.0.0', 8000, debug=True)
if __name__ == '__main__':
    app.run('0.0.0.0', 8000)
